import angular, {module} from 'angular';
import navheaderTemplate from './navheadertemplate.html';
import navheaderController  from './navheadercontroller';

export function navHeader(){
  return {
  	scope: {},
  	bindToController: {
  		'search' : '='
  	},
  	require: '^gallery',
  	link: function($scope, $element, $attrs, $ctrl){
  		$scope.onSubmit = () => {
  			if($scope.header.search.trim() !== '') {
			  	$ctrl.refreshApi();
			  	$scope.header.magnifier = false;
		 	}
		}

		$scope.onReset = () => {
			$ctrl.resetApi();
		  	$scope.header.magnifier = true;
		}
  	},
    restrict: 'E',
    template: navheaderTemplate,
    controller: navheaderController,
    controllerAs: 'header'
  };
}

export default module('navheader', []).directive('navheader', navHeader).name;
