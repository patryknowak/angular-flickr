import angular, {module} from 'angular';
import bigphotoTemplate from './bigphototemplate.html';
import bigphotoController  from './bigphotocontroller';

function bigphoto(){
  return {
  	scope: {},
  	bindToController: {
  		'current': '=',
  		'currPage': '=',
  		'currPhoto': '='
  	},
  	require: '^gallery',
  	link: function($scope, $element, $attrs, $ctrl){
      $scope.onPrevClick = $ctrl.prevPhoto;
      $scope.onNextClick = $ctrl.nextPhoto;
  	},
    restrict: 'E',
    template: bigphotoTemplate,
    controller: bigphotoController,
    controllerAs: 'big'
  };
}

export default module('bigphoto', []).directive('bigphoto', bigphoto).name;
