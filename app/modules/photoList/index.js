import angular, {module} from 'angular';
import photolistTemplate from './photolisttemplate.html';
import photolistController  from './photolistcontroller';

export function photolist(){
  return {
  	scope: {},
    bindToController: {
      'list': '=',
      'current': '='
    },
  	require: '^gallery',
  	link: function($scope, $element, $attrs, $ctrl){

      $scope.onHandClick= function(index){
        console.log(index);
        $ctrl.changeCurrent(index)
      }
  	},
    restrict: 'E',
    template: photolistTemplate,
    controller: photolistController,
    controllerAs: 'photo'
  };
}

export default module('photolist', []).directive('photolist', photolist).name;
