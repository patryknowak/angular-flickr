import angular, {module} from 'angular';
import mainTemplate from './main.html';
import mainController  from './maincontroller';

export function main($document){
  return {
  	scope: {},
  	link: function($scope){
  		$document[0].addEventListener('keydown', function(e){
  			if(e.which === 39) {
  				$scope.main.nextPhoto()
  			}

  			if(e.which === 37) {
  				$scope.main.prevPhoto()
  			}

  			$scope.$apply();
  		})
  	},
    restrict: 'E',
    template: mainTemplate,
    controller: mainController,
    controllerAs: 'main'
  };
}

main.$inject = ['$document'];

export default module('main', []).directive('gallery', main).name;
