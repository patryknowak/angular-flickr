export default function mainController(service) {
  this.title = "Flickr";

  this.photoId = 0;
  this.page = 1;
  this.pages = 0;
  this.perPage = 15;
  this.search = '';
  this.photos = [];

  this.readyLinks = [];

  const makeLinks = (element) => {
  	this.readyLinks.push({
  		small: service.makePhoto(element, 'q'),
  		big: service.makePhoto(element, 'b')
  	})
  }

  const fetchPhotos = () => {
	  service.getPhotos(this.page, this.search)
	  	.then((data)=> {
	  		data.photos.photo.map(makeLinks);
	  		this.photos = this.readyLinks.map((el)=> el.small);
			this.current = this.readyLinks[0].big;
			this.pages = data.photos.pages;
	  	})
  }

  fetchPhotos();

  this.changeCurrent = (index) => {
  	this.photoId = index;
  	this.current = this.readyLinks[this.photoId].big;
  };

  this.refreshApi = () => {
  	this.pages = 0;
  	this.page = 1;
  	this.photoId = 0;
  	this.readyLinks = [];

  	fetchPhotos();
  }

  this.resetApi = () => {
  	this.pages = 0;
  	this.page = 1;
  	this.readyLinks = [];
  	this.search = '';
  	this.photoId = 0;

  	fetchPhotos();
  }
  

  this.changePage = (page, photoId) => {
  	this.readyLinks = [];
  	this.page = page;
  	this.photoId = 0;

  	fetchPhotos();
  }

  this.nextPage = () => {
  	this.changePage(++this.page)
  }

  this.prevPage = () => {
  	this.changePage(--this.page)
  }

  this.nextPhoto = () => {
  	if(this.photoId === this.perPage-1 && (this.page+1 <= this.pages)) { this.changePage(this.page+1); return false;}
  	if(this.photoId < this.perPage) {++this.photoId; this.current = this.readyLinks[this.photoId].big }
  }

  this.prevPhoto = () => {
  	if(this.photoId === 0 & this.page > 1) this.changePage(this.page-1); 
  	else if(this.photoId > 0) {this.photoId--; this.current = this.readyLinks[this.photoId].big}
  }
}

mainController.$inject = ['flickrService'];