import angular, {module} from 'angular';
import paginationTemplate from './paginationtemplate.html';
import paginationController  from './paginationcontroller';

export function pagination(){
  return {
  	scope: {},
  	bindToController: {
  		'pages' : '=',
  		'currPage' : '='
  	},
  	require: '^gallery',
  	link: function($scope, $element, $attrs, $ctrl){

      $scope.onPaginationClick = (p) => {
      	if(p !== '...' && $scope.page.currPage !== p) {
      		console.log(p);
      		$ctrl.changePage(p);
      		$scope.page.navList = $scope.page.buildPagination($ctrl.pages, $ctrl.page);
      	}
      };

      $scope.onPrevPage = () => {
      	if($scope.page.currPage > 1) {
	      	$ctrl.changePage($scope.page.currPage-1);
	      	$scope.page.navList = $scope.page.buildPagination($ctrl.pages, $ctrl.page);
      	}
      }

      $scope.onNextPage = () => {
      	$ctrl.changePage($scope.page.currPage+1);
      	$scope.page.navList = $scope.page.buildPagination($ctrl.pages, $ctrl.page);
      }
  	},
    restrict: 'E',
    template: paginationTemplate,
    controller: paginationController,
    controllerAs: 'page'
  };
}

export default module('pagination', []).directive('pagination', pagination).name;
