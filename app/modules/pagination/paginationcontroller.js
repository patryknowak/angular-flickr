export default function paginationController() {
  const h = 4;

  this.buildPagination = (pages, current) => {
  	let pageList = [];
  	let pt = pages/h;

  	if(pages === 0) {
  		return;
  	}

  	pageList.push(1);

  	if(pages < h*2+1 ) {
  		for(let i =2; i<= pages; i++) {
  			pageList.push(i);
  		}
  	} 

  	if(current < h && pages > h*2) {
  		pageList.push(2);
  		pageList.push(3);
  		pageList.push(4);
  	}

  	if(current > 3 && pages > h*2) {
  		pageList.push('...');
  		pageList.push(current - 1);
  		pageList.push(current);
  		if(current + 2 < pages) {
  			pageList.push(current + 1);
  		}
  	}
  	
  	if(pt > 2 && (pages > current)) {
  		if(current + 3 < pages) {
  			pageList.push('...');
  		}
  		if(current < pages - 1) {
  			pageList.push(pages - 1);
  		}
  		pageList.push(pages);
  	}


  	return pageList;
  }

  this.navList = this.buildPagination(this.pages, this.currPage);
}