import angular, {module} from 'angular';
import main from './modules/main/';
import navheader from './modules/navheader/';
import bigphoto from './modules/bigphoto/';
import photolist from './modules/photolist/';
import pagination from './modules/pagination/';

import flickrService from './services/flickrservice.js';

import './style/main.scss';


module('gallery', [main, navheader, bigphoto, photolist, pagination, flickrService]);

document.addEventListener('DOMContentLoaded',function(){
    angular.bootstrap(document.body, ['gallery']);
});