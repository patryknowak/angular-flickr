import angular, {module} from 'angular';


// https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=0daed8ce23060668fa0d55c247019c00
//&per_page=15&format=json&nojsoncallback=1&api_sig=793af05f07034abb22a1302e9679a965
export default function flickrService($http){

	function getPhotos(page, search){
		var method = 'flickr.photos.getRecent', text = 'aaa';
		if(search){
			method = 'flickr.photos.search';
			text = search;
		}
		return $http({
			method: 'GET',
			url: 'https://api.flickr.com/services/rest/',
			params: {
				method: method,
				api_key: '8a819eb73ea8b28d1dd4b52e218a7bb5',
				text: text,
				per_page: 15,
				page: page,
				format: 'json',
				nojsoncallback: 1
			}
		}).then((response) => response.data)
	}

	function makePhoto(obj, size){
		return `https://farm${obj.farm}.staticflickr.com/${obj.server}/${obj.id}_${obj.secret}_${size}.jpg`
	}

	return {
		getPhotos: getPhotos,
		makePhoto: makePhoto
	}
}


flickrService.$inject = ['$http'];

export default angular.module('gallery.service', []).factory('flickrService', flickrService).name;